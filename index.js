const QS = require('./emfit.js')
const request = require('request')
var qs = new QS()

var existheaders = {"Authorization": "Bearer TOTALLYNOTAPASSWORD", "Content-Type": "application/json"}
var formData = {
    name:     'custom', 
    active: 'true'
 };
 
 request.post(
   {
     url: 'https://exist.io/api/1/attributes/acquire/',
     body: formData,
     json: true,
     headers: existheaders,
   },
   function (err, httpResponse, body) {
     console.log(err, body);
   }
 );

qs.login('ardnat@gmail.com', 'TOTALLYNOTAPASSWORD').then(function(data) {
    console.log("############################")
    console.log(data.token)
    token = data.token
    qs.statuses() // token set automatically
    qs.user().then(function(data) {
        console.log(data.device_settings.length + ' devices found for ' + data.user.email)
      
        // get latest data for first device found
        let deviceId = data.device_settings[0].device_id
        var d = new Date();
        d.setMonth(d.getMonth() - 1);
        qs.trends(deviceId,formatDate(d),formatDate(new Date()),3, token).then(function (sleep) {
          // dump all data
          for (i=0; i<sleep.data.length;i++) {
            var formData = {
                name:     'cycle_distance', 
                date: sleep.data[i].date, 
                value:         sleep.data[i].hrv_rmssd_morning
             };
            
             request.post(
                {
                  url: 'https://exist.io/api/1/attributes/update/',
                  body: formData,
                  json: true,
                  headers: existheaders,
                },
                function (err, httpResponse, body) {
                  console.log(err, body);
                }
              );
            console.log(sleep.data[i].date)
            sleepFor(2000);
            
          }
         
        })
      
      }).catch(function(error){
        console.error(error)
      })

  })

  function sleepFor( sleepDuration ){
    var now = new Date().getTime();
    while(new Date().getTime() < now + sleepDuration){ /* do nothing */ } 
}

function formatDate(date) {
    var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2) 
        month = '0' + month;
    if (day.length < 2) 
        day = '0' + day;

    return [year, month, day].join('-');
}